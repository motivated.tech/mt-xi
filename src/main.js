// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import chat from './chat'

import axios from 'axios'
Vue.prototype.$http = axios

if (store.state.token) {
  chat.socket.open()
  chat.socket.emit('login', store.state.user)
}

Vue.prototype.$socket = chat.socket

Vue.config.productionTip = true

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
