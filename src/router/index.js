import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import ChatComponent from '@/components/chat-component'
import TasksComponent from '@/components/tasks-component'
import Login from '@/components/login'

Vue.use(Router)

function loginCheck (to, from, next) {
  if (store.state.token) {
    next()
  } else {
    next('/login')
  }
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ChatComponent',
      component: ChatComponent,
      beforeEnter: loginCheck
    },
    {
      path: '/tasks',
      name: 'TasksComponent',
      component: TasksComponent,
      beforeEnter: loginCheck
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
