import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    user: {},
    users: [],
    messages: [],
    goals: [],
    showSettings: false,
    token: '',
    currentGroup: ''
  },
  mutations: {
    setUser (state, user) {
      state.user = user
    },
    addUser (state, user) {
      state.users.push(user)
    },
    newMessage (state, msg) {
      state.messages.push(msg)
    },
    addGoal (state, goal) {
      state.goals.push(goal)
    },
    toggleGoal (state, id) {
      let index = state.goals.findIndex((g) => {
        return g.id === id
      })
      state.goals[index].status = !state.goals[index].status
    },
    toggleSettings (state) {
      state.showSettings = !state.showSettings
    },
    setToken (state, token) {
      state.token = token
    },
    setGroup (state, groupId) {
      state.currentGroup = groupId
    }
  }
})
