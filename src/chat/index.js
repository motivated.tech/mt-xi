import io from 'socket.io-client'
import store from '@/store'
const socket = io.connect('http://api.motivated.tech', { autoConnect: false })
// const socket = io.connect('/api', { autoConnect: false })

socket.on('message', (data) => {
  store.commit('newMessage', data)
})

export default {
  socket
}
